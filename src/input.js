import React from "react";
import "./searchInput.less"
import {MdSearch} from 'react-icons/md';
import {MdSettingsVoice} from 'react-icons/md';

const Input = () => {
  return (
    <p className={'search-input'}>
      <MdSearch className={'MdSearch search-icon icon'}></MdSearch>
      <input className={"input"} type={"text"} />
      <MdSettingsVoice className={'MdSettingsVoice voice icon'}></MdSettingsVoice>
    </p>
  );
};

export default Input;