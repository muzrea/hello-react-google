import React from 'react';
import './Search.less';
import Input from "./input";
import Button from "./button";

const Search = () => {
  return (
    <section className='search'>
      <header>
        <img src="https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png"
                   alt="Google Logo"/>
      </header>
      <div>
        <Input/>
      </div>
      <div>
        <Button  value={"Google Search"}/>
        <Button value={"I'm Feeling Lucky"}/>
      </div>
    </section>
  );
};

export default Search;