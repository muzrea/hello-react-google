import React from "react";
import "./footer.less";

const Footer = (props) => {
  return (
    <footer className={"footer"}>
      <p className={"footer-p title"}><span>Hong Kong</span></p>
      <hr/>
      <p className={"footer-p"}>
        <a href={'https://ads.google.com/intl/en_hk/home/?subid=ww-ww-et-g-awa-a-g_hpafoot1_1!o2&utm_source=google.com&utm' +
        '_medium=referral&utm_campaign=google_hpafooter&fg=1'}>Advertising</a>
        <a href={'https://ads.google.com/intl/en_hk/home/?subid=ww-ww-et-g-awa-a-g_hpafoot1_1!o2&utm_source=google.com&utm_' +
        'medium=referral&utm_campaign=google_hpafooter&fg=1'}>Bussiness</a>
        <a href={'https://ads.google.com/intl/en_hk/home/?subid=ww-ww-et-g-awa-a-g_hpafoot1_1!o2&utm_source=google.com&utm' +
        '_medium=referral&utm_campaign=google_hpafooter&fg=1'}>About</a>
        <a href={'https://ads.google.com/intl/en_hk/home/?subid=ww-ww-et-g-awa-a-g_hpafoot1_1!o2&utm_source=google.com&utm_' +
        'medium=referral&utm_campaign=google_hpafooter&fg=1'}>How search works</a>
      </p>
    </footer>
  );
};

export default Footer;