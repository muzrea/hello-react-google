import React from 'react';
import './App.less';
import Menu from "./Menu";
import Search from './Search';
import LanguageTransfer from "./LanguageTansfer";
import Footer from "./footer";

const App = () => {
  return (
    <div className='App'>
      <Menu/>
      <Search/>
      <LanguageTransfer/>
      <Footer/>
    </div>
  );
};

export default App;